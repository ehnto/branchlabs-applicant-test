<?php

abstract class AbstractModel
{
    protected $_table;
    protected $_pk;
    protected $_attributes;
    protected $_db;

    public function __construct() {
        $this->_db = new MysqlAdapter();
    }

    public function load (int $id) {
        $this->unload();

        //Sanitation is provided by the int typecast in the method signature
        $query = $this->_db
            ->query("SELECT * FROM {$this->_table} WHERE {$this->_pk} = {$id} LIMIT 1");
        $query->execute();
        $contact = $query->fetch(PDO::FETCH_ASSOC);

        if(!$contact) {
            throw new \Exception('Could not load model');
        }

        foreach($contact as $attributeName => $attributeValue) {
            $this->_attributes[$attributeName] = $attributeValue;
        }

        return $this;
    }

    public function unload() {
        $this->_attributes = null;
    }

    public function save() {
        $updatePk = false;
        $whereClause = '';

        if($this->getData($this->_pk)) {
            $preparedStatement = "UPDATE {$this->_table} SET ";
            $whereClause .= " WHERE {$this->_pk}={$this->getData($this->_pk)}";
        } else {
            $preparedStatement = "INSERT INTO {$this->_table} SET";
            $updatePk = true;
        }

        foreach ($this->getData() as $attributeName => $attributeValue) {
            $preparedStatement .= " $attributeName=:$attributeName,";
        }

        $preparedStatement = rtrim($preparedStatement, ',');
        $preparedStatement .= $whereClause;

        $statement = $this->_db->getConnection()
                  ->prepare($preparedStatement)
                  ->execute($this->getData());

        if($updatePk) {
            $this->setData($this->_pk, $this->_db->getConnection()->lastInsertId());
        }
        
        return $this;
    }
    
    public function delete($id = false) {
        if($id === false || $id === null) {
            $id = $this->getData($this->_pk);
        }
        
        if($id || $id === 0) {
            $statement = $this->_db->query("DELETE FROM {$this->_table} WHERE {$this->_pk}={$id}");
            $statement->execute();
        }

        return $this;
    }

    public function getData($attributeName = false) {
        if(!$attributeName) {
            return $this->_attributes;
        }

        if(!isset($this->_attributes[$attributeName])) {
            return null;
        }

        return $this->_attributes[$attributeName];
    }

    public function setData($attributeName, $attributeValue = false) {
        if(is_array($attributeName)) {
            foreach($attributeName as $attributeName => $attributeValue) {
                $this->_attributes[$attributeName] = $attributeValue;
            }
            
            return $this;
        }

        $this->_attributes[$attributeName] = $attributeValue;

        return $this;
    }
}