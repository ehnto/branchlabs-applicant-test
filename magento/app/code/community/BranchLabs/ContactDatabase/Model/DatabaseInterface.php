<?php

interface BranchLabs_ContactDatabase_Model_DatabaseInterface {
    public function connect();
    public function query(string $queryString);
}