<?php


Class BranchLabs_ContactDatabase_Model_Contact extends BranchLabs_ContactDatabase_Model_AbstractModel
{
	protected $_table = "contacts";
	protected $_pk	  = "id";
}