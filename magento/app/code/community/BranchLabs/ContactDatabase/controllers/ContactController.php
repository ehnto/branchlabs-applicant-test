<?php

class BranchLabs_ContactDatabase_ContactController extends Mage_Core_Controller_Front_Action
{
    public function viewAction() {
        $contactId = (int) $this->getRequest()->getParam('id');
        $contact = Mage::getModel('contact_database/contact')
                                 ->load($contactId);

        try {
            //$this->_addDefaultLayoutHandles();
            //$this->renderLayout();

            /* In the interest of time we are just printing the results, real world scenario we would go through and add
             * the templates, styles and layout xml files to piece this all together */

            var_dump($contact);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    protected function _addDefaultLayoutHandles() {
        $this->getLayout()
             ->getUpdate()
             ->addHandle('contact_default');

        $this->loadLayoutUpdates();
    }
}