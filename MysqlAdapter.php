<?php

class MysqlAdapter implements DatabaseInterface {
    protected $_connection = false;

    public function __construct() {
        $this->connect();

        $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function connect() {
        if(!$this->hasConnection()) {
            $this->_makeConnection();
        }

        return $this;
    }

    protected function _makeConnection() {
        //Lets pretend we get these from configuration files
        try {
            $connection = new PDO(
                'mysql:host=localhost;dbname=branchlabs',
                'development',
                'development'
            );

            $this->_setConnection($connection);
        } catch (PDOException $exception) {
            print ("I couldn't make a connection. I will try harder next time." . PHP_EOL);
        }

        return $this;
    }

    public function hasConnection() {
        return ($this->_connection instanceof PDO);
    }

    public function getConnection() {
        return $this->_connection;
    }

    protected function _setConnection(PDO $connection) {
        $this->_connection = $connection;
        return $this;
    }

    public function query($query) {
        return $this->getConnection()->query($query);
    }
}